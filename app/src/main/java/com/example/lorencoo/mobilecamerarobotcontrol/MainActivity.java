package com.example.lorencoo.mobilecamerarobotcontrol;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.button_bluetooth_on)
    public void openBluetoothActivity() {
        startActivity(new Intent(this, Bluetooth_connection_activity.class));
    }
    @OnClick(R.id.button_manual_control)
    public void openManualControlActivity(){
        startActivity(new Intent(this, Manual_control_activity.class));
    }
}
