package com.example.lorencoo.mobilecamerarobotcontrol;

import android.annotation.SuppressLint;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Set;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class Bluetooth_connection_activity extends AppCompatActivity {

    private static final int REQUEST_ENABLE_BT = 0;
    private static final int REQUEST_DISCOVER_BT = 0;

    BluetoothAdapter bluetoothAdapter;

    @BindView(R.id.status_bluetooth_tv)
    TextView textStatusBluetooth;

    @BindView(R.id.paired_bluetooth_tv)
    TextView pairedBluetoothTv;

    @BindView(R.id.status_bluetooth_image)
    ImageView imageStatusBluetooth;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bluetooth_connection_activity);
        ButterKnife.bind(this);
        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        //check if bluetooth is available or not
        if (bluetoothAdapter == null) {
            textStatusBluetooth.setText("Bluetooth is not available");
        } else {
            textStatusBluetooth.setText("Bluetooth is available");
        }
        if (bluetoothAdapter.isEnabled()) {
            setImageBluetoothOn();
        } else {
            setImageBluetoothOff();
        }
    }

    @OnClick(R.id.turn_on_bluetooth_btn)
    public void turnOnBtn() {
        if (!bluetoothAdapter.isEnabled()) {
            showToast("Turning On bluetooth....");
            Intent intentBtEnable = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(intentBtEnable, REQUEST_ENABLE_BT);
        } else {
            setImageBluetoothOn();
            showToast("Bluetooth is already on");
        }
    }

    @SuppressLint("SetTextI18n")
    @OnClick(R.id.turn_off_bluetooth_btn)
    public void turnOffBtn() {
        if (bluetoothAdapter.isEnabled()) {
            bluetoothAdapter.disable();
            showToast("Turning Bluetooth off");
            setImageBluetoothOff();
            textStatusBluetooth.setText("Bluetooth is not available");
        } else {
            setImageBluetoothOff();
            showToast("Bluetooth is already off");
        }
    }

    @OnClick(R.id.discoverable_bluetooth_btn)
    public void discoverableClick() {
        if (!bluetoothAdapter.isDiscovering()) {
            showToast("Making Your Device Discoverable");
            Intent discoverableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
            startActivityForResult(discoverableIntent, REQUEST_DISCOVER_BT);
        }
    }

    @SuppressLint("SetTextI18n")
    @OnClick(R.id.paired_bluetooth_btn)
    public void pairedClick() {
        if (bluetoothAdapter.isEnabled()) {
            pairedBluetoothTv.setText("Paired Devices");
            Set<BluetoothDevice> devices = bluetoothAdapter.getBondedDevices();
            for (BluetoothDevice device : devices) {
                pairedBluetoothTv.append("\nDevice " + device.getName() + "," + device);
            }
        } else {
            //bluetooth is off so can't get paired devices
            showToast("Turn on bluetooth to get paired devices");
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        switch ((requestCode)) {
            case REQUEST_ENABLE_BT:
                if (resultCode == RESULT_OK) {
                    setImageBluetoothOn();
                    showToast("Bluetooth is on");
                } else {
                    showToast("could't on bluetooth");
                }
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void showToast(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    private void setImageBluetoothOn() {
        imageStatusBluetooth.setImageResource(R.drawable.ic_action_on);
    }

    private void setImageBluetoothOff() {
        imageStatusBluetooth.setImageResource(R.drawable.ic_action_off);

    }
}

